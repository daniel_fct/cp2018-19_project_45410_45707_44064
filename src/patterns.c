#include <string.h>
#include <assert.h>
#include "patterns.h"
#include <cilk/cilk.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

//////auxiliar functions/////////

double upPass(void *array, int lo, int hi, size_t sizeJob){

	if(hi-lo <= 0){
		return *(double*)(array + hi * sizeJob);	
	}else{
		cilk_spawn upPass(array, lo, (hi+lo)/2, sizeJob);
		double right = upPass(array, (hi+lo)/2+1, hi, sizeJob);
		cilk_sync;
		*(double*)(array + hi*sizeJob) = *(double*)(array+((hi+lo)/2)*sizeJob) + right;
		return *(double*)(array + hi*sizeJob);
	}
}

void downPass(double num, void *array, int lo, int hi, size_t sizeJob){

	if(hi-lo != 0){	
		double temp = num - *(double*)(array+((hi+lo)/2)*sizeJob);
		double mid = *(double*)(array+((hi+lo)/2)*sizeJob);
		*(double*)(array+((hi+lo)/2)*sizeJob) = *(double*)(array+(hi)*sizeJob) - temp;

		cilk_spawn downPass(mid, array, lo, (hi+lo)/2, sizeJob);
		downPass(temp, array, (hi+lo)/2+1, hi, sizeJob);
	}
}

void farmer(int ownPos, size_t nWorkers, volatile void *numThreads, volatile void *aux, void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2), volatile void *current){
	
	*(int*)numThreads = *(int*)numThreads + 1;
	
	while(1){
		
		if(*(int*)(aux + ownPos*sizeof(int)) != -1){
			worker(dest + ( *(int*)(aux + ownPos*sizeof(int)) ) * sizeJob, src + ( *(int*)(aux + ownPos*sizeof(int)) ) * sizeJob);
			*(int*)(aux + ownPos*sizeof(int)) = -1;
			//printf(" ");
		}
        
		if(*(int*)current == nJob && *(int*)(aux + ownPos*sizeof(int)) == -1){
			break;		
		}	
	}
}

//////auxiliar functions/////////

void map (void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2)) {
    
    assert (dest != NULL);
    assert (src != NULL);
    assert (worker != NULL);

   if(nJob < 125000){

   		for (int i=0; i < nJob; i++) 
        worker(dest + i * sizeJob, src + i * sizeJob);

   } else {

    cilk_for (int i=0; i < nJob; i++) 
        worker(dest + i * sizeJob, src + i * sizeJob);

    }
}

void reduce (void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2, const void *v3)) {
    /* To be implemented */
    assert (dest != NULL);
    assert (src != NULL);
    assert (worker != NULL);

	if(nJob == 1){
		memcpy(dest, src, sizeJob);	
	}else if(nJob == 2){
		worker(dest, src, src + sizeJob);
	}else if(nJob < 150000){
		
	}else if(nJob > 2){
		if(nJob < 150000){
			//-------------------serial implementation------------------------
        		memcpy (dest, src, sizeJob);
        		for (int i = 1;  i < nJob;  i++)
        		    worker(dest, dest, src + i * sizeJob);
			//-------------------serial implementation------------------------
		}else{
			int numberOfIterations = abs((log (nJob)) / (log (2)));
		
			if(nJob%2 == 0)
				numberOfIterations--;
			else
				nJob++;
			
			int numberOfCalculation = nJob/2;
 		
			void *array;
			array = malloc((nJob/2)*sizeof(sizeJob));
		
    			cilk_for (int i = 0;  i < nJob;  i += 2){
				worker(array + (i/2)*sizeJob, src + (i * sizeJob), src + ((i+1) * sizeJob));
			}
		
			for(int i = 0; i < numberOfIterations; i++){
				cilk_for(int j = 0 ; j < numberOfCalculation; j+=2){		
					worker(array + (j/2) * sizeJob, array + j * sizeJob, array + (j+1) * sizeJob);
				}
				numberOfCalculation = numberOfCalculation/2;
			}

		
			memcpy(dest, array, sizeJob);
			free((void*)array);
		}
		

	}
	
}



void scan (void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2, const void *v3)) {
    /* To be implemented */
    assert (dest != NULL);
    assert (src != NULL);
    assert (worker != NULL);

	if(nJob == 1){
		memcpy(dest, src, sizeJob);	
	}else if(nJob == 2){
		memcpy(dest, src, sizeJob);
		worker(dest + sizeJob, src, src + sizeJob);
	}else if(nJob > 2){
		memcpy (dest, src, nJob*sizeJob);

		upPass(dest, 0, nJob-1, sizeJob);
		downPass(*(double*)(dest + (nJob-1)*sizeJob), dest, 0, nJob-1, sizeJob);
	}
	/*
	//-------------------serial implementation------------------------\\
	if (nJob > 1) {
		memcpy (dest, src, sizeJob);
		for (int i = 1;  i < nJob;  i++)
		worker(dest + i * sizeJob, src + i * sizeJob, dest + (i-1) * sizeJob);
	}
	//-------------------serial implementation------------------------
	*/
	
}

int pack (void *dest, void *src, size_t nJob, size_t sizeJob, const int *filter) {
    
    int pos = 0;

    if(nJob < 500000){
	
	for (int i=0; i < nJob; i++) {
        if (filter[i]) {
            memcpy (dest + pos * sizeJob, src + i * sizeJob, sizeJob);
            pos++;
        }
    }

    } else {

    cilk_for (int i=0; i < nJob; i++) {
        if (filter[i]) {
            memcpy (dest + pos * sizeJob, src + i * sizeJob, sizeJob);
            pos++;
        }
    }
}
    return pos;
}

void gather (void *dest, void *src, size_t nJob, size_t sizeJob, const int *filter, int nFilter) {

	if(nJob < 500000){

		for (int i=0; i < nFilter; i++) {

       		memcpy (dest + i * sizeJob, src + filter[i] * sizeJob, sizeJob);
       		
    	}

	} else {

		cilk_for (int i=0; i < nFilter; i++) {

       		memcpy (dest + i * sizeJob, src + filter[i] * sizeJob, sizeJob);

    	}
	}
}

void scatter (void *dest, void *src, size_t nJob, size_t sizeJob, const int *filter) {

	if(nJob < 500000){
	
		for (int i=0; i < nJob; i++){
	 		
	 		if(*(double*)(dest + filter[i] * sizeJob) == 0){  
        	
				memcpy (dest + filter[i] * sizeJob, src + i * sizeJob, sizeJob);
	
			}else { 

				*(double*)(dest + filter[i]*sizeJob) += *(double*)(src + i * sizeJob);	
			} 
   		}

    } else {

    	cilk_for (int i=0; i < nJob; i++) {
    
			if(*(double*)(dest + filter[i] * sizeJob) == 0){  
        	
				memcpy (dest + filter[i] * sizeJob, src + i * sizeJob, sizeJob);
	
			} else {  

				*(double*)(dest + filter[i]*sizeJob) += *(double*)(src + i * sizeJob);
		
			}
    	}
	}
}

/*void scatter2 (void *dest, void *src, size_t nJob, size_t sizeJob, const int *filter, void (*worker)(void *v1, const void *v2, const void *v3)) {

	
	 if(nJob < 500000){
	
	 	for (int i=0; i < nJob; i++){
	 		if(*(double*)(dest + filter[i] * sizeJob) == 0){  
        	
				memcpy (dest + filter[i] * sizeJob, src + i * sizeJob, sizeJob);
	
			} else {  

				worker(dest + filter[i] * sizeJob, dest + filter[i]*sizeJob, src + i * sizeJob);
			} 
    	}

    } else {

    	cilk_for (int i=0; i < nJob; i++) {
    
			if(*(double*)(dest + filter[i] * sizeJob) == 0){  
        	
				memcpy (dest + filter[i] * sizeJob, src + i * sizeJob, sizeJob);
	
			} else {  

				worker(dest + filter[i] * sizeJob, dest + filter[i]*sizeJob, src + i * sizeJob);

			}
    	}
    
	}
}*/

void pipeline (void *dest, void *src, size_t nJob, size_t sizeJob, void (*workerList[])(void *v1, const void *v2), size_t nWorkers) {
    
   cilk_for (int i=0; i < nJob; i++) {
        memcpy (dest + i * sizeJob, src + i * sizeJob, sizeJob);
        for (int j = 0;  j < nWorkers;  j++)
            workerList[j](dest + i * sizeJob, dest + i * sizeJob);
    }
}


void farm (void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2), size_t nWorkers) {
		
	volatile void *numThreads;
	numThreads = malloc(sizeof(int));
	*(int*)numThreads = 0;

	volatile void *current;
	current = malloc(sizeof(int));
	*(int*)current = 0;

	volatile void *farmersDone;
	farmersDone = malloc(sizeof(int));
	*(int*)farmersDone = 0;

	//fazer free do malloc
	volatile void *aux;
	aux = malloc((nWorkers)*sizeof(int));
	for(int i = 0; i < nWorkers; i++){
		*(int*)(aux + i * sizeof(int)) = -1;
	}

	for(int j = 0; j < nWorkers; j++){
		cilk_spawn farmer(j, nWorkers, numThreads, aux, dest, src, nJob, sizeJob, worker, current);
	}

	while(1){
        
		if(*(int*)current == nJob){
			*(int*)farmersDone = 1;

            		for(int k = 0; k < *(int*)numThreads; k++){
				if(*(int*)(aux + k*sizeof(int)) != -1){
					*(int*)farmersDone = 0;
                    			break;
				}
            		}
            
			if(*(int*)farmersDone == 1){
				break;
            		}
        	}
        
		for(int l = 0; l < *(int*)numThreads; l++){
			if(*(int*)current == nJob){
				break;
            		}
            
			if(*(int*)(aux + l*sizeof(int)) == -1){
				*(int*)(aux + l*sizeof(int)) = *(int*)current;
                		*(int*)current = *(int*)current + 1;
			}		
		}
	}
	free((void*)numThreads);
	free((void*)current);
	free((void*)farmersDone);
	free((void*)aux);
	
	//-------------------serial implementation------------------------
	//map (dest, src, nJob, sizeJob, worker);
	//-------------------serial implementation------------------------

}
